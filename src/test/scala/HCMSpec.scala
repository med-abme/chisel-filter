package util

import java.nio.file.StandardCopyOption.REPLACE_EXISTING
import java.nio.file.{Files, Paths}

import chisel3._
import chisel3.iotesters
import chisel3.iotesters.PeekPokeTester
import firrtl.FileUtils
import org.scalatest.{FreeSpec, Matchers}


class HCMTester(c: HCM) extends PeekPokeTester(c) {

  poke(c.io.kernel(0), (-1).S)
  poke(c.io.kernel(0), (-1).S)
  poke(c.io.kernel(1), (-2).S)
  poke(c.io.kernel(2), (-1).S)
  poke(c.io.kernel(3), 0.S)
  poke(c.io.kernel(4), 0.S)
  poke(c.io.kernel(5), 0.S)
  poke(c.io.kernel(6), 1.S)
  poke(c.io.kernel(7), 2.S)
  poke(c.io.kernel(8), 1.S)

  for (i <- 0 until 9 ) {
    //println (peek(c.conv.io.kernel(i)) .toString)
  }

  for(addr <- 0 until 256) {
    poke(c.io.address, addr)
    step(1)
    println(f"peek from $addr ${peek(c.io.value.a)}%x")
    poke(c.io.inBuffer(addr), addr )

    //println("Result is" + peek(c.io.inBuffer(addr)).toString)

  }

  step (1)

  for(i <- 0 until 256) {
    println (" Result is: " + "[" + i + "]" + peek(c.conv.io.out(i)) .toString )
    step(1)
  }



  println("Done compiling")
}


class HCMSpec extends  FreeSpec with Matchers {


  "memory loading should be possible with complex memories" - {

    val targetDirName = "test_run_dir/complex_mem_test"
    FileUtils.makeDirectory(targetDirName)

    val path1 = Paths.get(targetDirName + "/f_a")


    Files.copy(getClass.getResourceAsStream("/f"), path1, REPLACE_EXISTING)



    "should work with verilator" in {
      iotesters.Driver.execute(
        args = Array("--backend-name", "verilator", "--target-dir", targetDirName, "--top-name", "complex_mem_test"),
        dut = () => new HCM( memoryDepth = 256)
      ) { c =>
        new HCMTester(c)
      } should be(true)
    }
  }
}