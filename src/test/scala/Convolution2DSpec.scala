package util

import chisel3._
import chisel3.iotesters._
import org.scalatest.{FlatSpec, Matchers}



class Convolution2DTester (c: Convolution2D ) extends PeekPokeTester (c) {

  poke(c.io.in(0), 1.U)
  poke(c.io.in(1), 2.U)
  poke(c.io.in(2), 3.U)
  poke(c.io.in(3), 4.U)
  poke(c.io.in(4), 5.U)
  poke(c.io.in(5), 6.U)
  poke(c.io.in(6), 7.U)
  poke(c.io.in(7), 8.U)
  poke(c.io.in(8), 9.U)

  poke(c.io.kernel(0), (-1).S)
  poke(c.io.kernel(1), (-2).S)
  poke(c.io.kernel(2), (-1).S)
  poke(c.io.kernel(3), 0.S)
  poke(c.io.kernel(4), 0.S)
  poke(c.io.kernel(5), 0.S)
  poke(c.io.kernel(6), 1.S)
  poke(c.io.kernel(7), 2.S)
  poke(c.io.kernel(8), 1.S)

  step (1)
  for (k <- 0 until 9 ) {
     println (" Result is: " + "[" + k + "]" + peek(c.io.out(k)) .toString )
  }
}


class Convolution2DSpec extends FlatSpec with Matchers {
  behavior of "Convolution2DSpec"

  it should "compute Convolution2DSpec excellently" in {
    chisel3.iotesters.Driver(() => new Convolution2D(3,3)) { c => new Convolution2DTester(c)
    } should be(true)
  }
}