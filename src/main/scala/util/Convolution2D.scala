package util

import chisel3._

import scala.collection.mutable.ArrayBuffer


class Convolution2D(rows: Int, cols: Int) extends Module {

    val imgSize = rows * cols //find input matrix size

    val io = IO(new Bundle {

      val kernel = Input(Vec(9, SInt(32.W))) //kernel vector
      val in = Input(Vec(imgSize, SInt(32.W))) // input matrix vector
      val out = Output(Vec(imgSize, SInt(32.W))) //output matrix vector
      val valid = Output(Bool())
      val load = Input(Bool())
    })
    //variables declaration
    val kCols, kRows = 3
    val H = new ArrayBuffer[SInt]() //vector H initialization
    // find center position of kernel (half of kernel size)
    val kCenterX: Int = kCols / 2 //kernel center x
    val kCenterY: Int = kRows / 2 //kernel center y

    //empty output vector preparation: initialized to 0
    for (i <- 0 until imgSize) {
      H += 0.asSInt(32.W)
    }

    when(io.load) {

      for (i <- 0 until rows) { // rows
        for (j <- 0 until cols) { // columns

          for (m <- 0 until kRows) { // kernel rows
            val mm = kRows - 1 - m // row index of flipped kernel
            for (n <- 0 until kCols) { // kernel columns
              val nn = kCols - 1 - n // column index of flipped kernel

              // index of input signal, used for checking boundary
              val ii = i + (kCenterY - mm)
              val jj = j + (kCenterX - nn)

              // ignore input samples which are out of bound
              if (ii >= 0 && ii < rows && jj >= 0 && jj < cols) {
                H((i * rows) + j) += io.in((rows * ii) + jj) * io.kernel((kRows * mm) + nn)

              }

            }
          }
        }
      }

      io.valid := true.B
    }.otherwise {
      io.valid := false.B
    }
    io.out := H //affect vector H to output vector
  }
