package util

import chisel3._
import chisel3.util._
import chisel3.util.experimental.loadMemoryFromFile
import firrtl.annotations.MemoryLoadFileType


class MShape extends Bundle {  // specifying the file for such a memory
  val a = UInt(256.W)
  //val b = SInt(8.W)
  //val c = Bool()   // file contain boolean values in part c
}

class HCM( memoryDepth: Int) extends Module {
  val io = IO(new Bundle {
    val address = Input(UInt(log2Ceil(memoryDepth).W))
    val value = Output(new MShape)
    val inBuffer = Input(Vec(256, SInt(32.W)))
    val outBuffer = Output(Vec(256, SInt(32.W)))
    val kernel = Input(Vec(9,SInt(32.W)))

  })

  io.inBuffer.zip(io.outBuffer).foreach { case (in, out) =>
    val reg = RegInit(0.S(32.W))
    reg := in
    out := reg
  }

  io.inBuffer <> io.outBuffer

  val memory = Mem(memoryDepth, new MShape)
  loadMemoryFromFile(memory, "test_run_dir/complex_mem_test/f", MemoryLoadFileType.Binary)
  io.value := memory(io.address)
  val conv = Module(new Convolution2D(16, 16))
  conv.io.in := io.outBuffer
  conv.io.kernel := io.kernel
  conv.io.load := true.B
}